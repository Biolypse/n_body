import numpy as np
import vispy
import vispy.scene
from vispy.scene import visuals
from vispy import app
import sys

nb_part = int(sys.argv[1])
dt = float(sys.argv[2])
end = 50
end = int(end/dt)

x_positions = np.loadtxt("Datas/X.dat")
y_positions = np.loadtxt('Datas/Y.dat')
z_positions = np.loadtxt('Datas/Z.dat')

canvas = vispy.scene.SceneCanvas(keys='interactive', show=True)
view = canvas.central_widget.add_view()
view.camera = 'turntable'

scatter = visuals.Markers()
view.add(scatter)

axis = visuals.XYZAxis(parent=view.scene)


def positions(it, nb_part, x_positions, y_positions, z_positions):
    pos = []
    for i in range(nb_part):
        pos.append([x_positions[it, i], y_positions[it, i], z_positions[it, i]])
    return np.array(pos)


def update(ev):
    global it
    global scatter
    # print(it)
    if it != int(end - 1):
        it += 1
    else:
        it = 0
    scatter.set_data(positions(it, nb_part, x_positions,
                    y_positions, z_positions), size=3)


it = 0
timer = app.Timer()
for i in range(int(end/dt)):
    timer.connect(update)
timer.start(iterations=-1)
canvas.show()
app.run()
