MODULE = Functions.F90
FLAGS = -Wall -O3
SIMU = Main.F90
PLOT = visual.py

SUPPORTED_COMMANDS := simu plot opti
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
    COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
    $(eval $(COMMAND_ARGS):;@:)
endif

Functions_opti.o: $(MODULE)
	gfortran-8 $< $(FLAGS) -fopenmp -c

opti: $(SIMU) Functions_opti.o $(COMMAND_ARGS)
	gfortran-8 $< Functions.o $(FLAGS) -fopenmp -o Simulation
	./Simulation $(COMMAND_ARGS)



Functions.o: $(MODULE)
	gfortran-8 $< $(FLAGS) -c

simu: $(SIMU) Functions.o $(COMMAND_ARGS)
	gfortran-8 $< Functions.o $(FLAGS) -o Simulation
	./Simulation $(COMMAND_ARGS)



plot: $(PLOT) $(COMMAND_ARGS)
	python3 $< $(COMMAND_ARGS)

.PHONY: clean
clean:
	rm -rf *.o *.mod Main Simulation


