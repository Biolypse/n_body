
# N_body project

This project has for goals to simulate the exact gravitational interaction between N particles of equal mass (1/N).

Particles will have to be defined by their positions, speeds and accelerations. As initial condition, the particules's positions must randomley be in a sphere of radius 1 and a solid rotation along the z-axis which the norm is 1 by units of time.

A leap-frog methods will be used to integrate the system.

# TODO

- [x] Create uniformly N particles contained in a sphere of radius 1.
- [x] Apply a solid rotation along the z axis for each one of them.
- [x] Add the gravitational interaction.
- [x] Compute the new positions, speeds and accelerations with the Leap-frog.
- [x] Verify the stability of the integrator via the evolution of conservative quantities.
- [x] Parallelization of the algorithm.
- [ ] Speed-up/strategy/limitation and criticism of used's methods.
