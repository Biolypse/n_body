#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# license: CC BY-SA
# author: Nico
# version: 1.0
# email: nico.bellemont@gmail.com

import numpy as np
from matplotlib import pyplot as plt

potential_energy = np.array(np.loadtxt("Datas/POT.dat"))
kinetic_energy = np.array(np.loadtxt("Datas/KIN.dat"))

total = kinetic_energy+potential_energy

plt.figure()
plt.plot(kinetic_energy, label="kinetic")
plt.plot(potential_energy, label="potential")
plt.plot(total, label="total")
plt.legend()
plt.show()
