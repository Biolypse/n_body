MODULE Functions
    IMPLICIT NONE

    CONTAINS

        SUBROUTINE Initial_position(N, PX, PY, PZ, VX, VY, VZ, omega, DT)
            INTEGER, INTENT(IN) :: N
            REAL(kind=8), INTENT(IN) :: omega, DT
            REAL(kind=8), INTENT(OUT), DIMENSION(:) :: PX, PY, PZ, VX, VY, VZ
            INTEGER :: i
            REAL(kind=8) :: x, y, z
            DO i=1, N, 1
                x=1.
                y=1.
                z=1.
                DO WHILE (SQRT(x*x + y*y + z*z)>1)
                    CALL RANDOM_NUMBER(x)
                    CALL RANDOM_NUMBER(y)
                    CALL RANDOM_NUMBER(z)
                    x=x*2.-1.
                    y=y*2.-1.
                    z=z*2.-1.
                END DO
                VX(i) = -y * omega
                VY(i) = x * omega
                VZ(i) = 0.
                PX(i) = x + VX(i) * DT/2.
                PY(i) = y + VY(i) * DT/2.
                PZ(i) = z + VZ(i) * DT/2.
            END DO
        END SUBROUTINE Initial_position

        SUBROUTINE Acceleration(G, N, PX, PY, PZ, AX, AY, AZ, POT, eps)
            INTEGER, INTENT(IN) :: N
            REAL(kind=8), INTENT(IN) :: G, eps
            REAL(kind=8), INTENT(INOUT), DIMENSION(:) :: PX, PY, PZ
            REAL(kind=8), INTENT(OUT), DIMENSION(:) :: AX, AY, AZ
            REAL(kind=8), INTENT(INOUT) :: POT
            REAL(kind=8) :: X, Y, Z, R
            INTEGER :: i, j
            AX = 0.0
            AY = 0.0
            AZ = 0.0
            POT = 0.0
            !$OMP PARALLEL PRIVATE(X, Y, Z, R) REDUCTION(+: POT)
            !$OMP DO SCHEDULE(DYNAMIC, 100)
            DO i=1, N, 1
                DO j=1, i-1, 1
                    X = PX(j)-PX(i)
                    Y = PY(j)-PY(i)
                    Z = PZ(j)-PZ(i)
                    R = SQRT(X*X + Y*Y + Z*Z + eps*eps)
                    POT = POT - G / (N * N * R)
                    AX(j) = AX(j) + G * (PX(i)-PX(j)) / (R*R*R*real(N))
                    AX(i) = AX(i) + G * (PX(j)-PX(i)) / (R*R*R*real(N))

                    AY(j) = AY(j) + G * (PY(i)-PY(j)) / (R*R*R*real(N))
                    AY(i) = AY(i) + G * (PY(j)-PY(i)) / (R*R*R*real(N))

                    AZ(j) = AZ(j) + G * (PZ(i)-PZ(j)) / (R*R*R*real(N))
                    AZ(i) = AZ(i) + G * (PZ(j)-PZ(i)) / (R*R*R*real(N))
                END DO
            END DO
            !$OMP END DO
            !$OMP END PARALLEL
        END SUBROUTINE Acceleration

        SUBROUTINE  Leapfrog(AX, AY, AZ, VX, VY, VZ, PX, PY, PZ, DT)
            REAL(kind=8), INTENT(IN) :: AX, AY, AZ, DT
            REAL(kind=8), INTENT(INOUT) :: VX, VY, VZ
            REAL(kind=8), INTENT(INOUT) :: PX, PY, PZ

            VX = VX + AX * DT
            VY = VY + AY * DT
            VZ = VZ + AZ * DT
            PX = PX + VX * DT
            PY = PY + VY * DT
            PZ = PZ + VZ * DT
        END SUBROUTINE  Leapfrog
END MODULE
