#!/bin/bash

for i in {1..10000..100}
    do
        for j in {1..10000..100}
            do
                time=$(./Simulation 32768 0.01 $i $j)
                echo $i $j $time >> time.dat
            done
    done